$(document).ready(function () {
    $app.init();
});

var $app = (function () {
    "use strict";
    var $t = {
        init: function () {
            $t.initGrupos();
            $t.initNumeros();
            $t.updateJogos();
        },
        initGrupos: function () {
            $(".grupo-input").on("keyup change", function () {
                $t.updateJogos();
            });
        },
        initNumeros: function () {
            $(".numeros-input").on("keyup change", function () {
                $t.updateTernos();
            });
        },
        updateJogos: function () {
            var $jogosInputs = $(".jogos .jogo");
            $jogosInputs.each(function (i, e) {
                var $inputP = $(this);
                var $inputs = $inputP.find(".box-jogo input");

                $inputs.each(function (i, e) {
                    var $inputC = $(this);
                    var ref = $inputC.attr("data-ref");
                    $inputC.val($(ref).val().replace(/[^0-9]/g, ""));
                });
            });
        },
        updateTernos: function () {
            var n1 = $("#n1").val().replace(/[^0-9]/g, "");
            var n2 = $("#n2").val().replace(/[^0-9]/g, "");
            var n3 = $("#n3").val().replace(/[^0-9]/g, "");
            var n4 = $("#n4").val().replace(/[^0-9]/g, "");
            var n5 = $("#n5").val().replace(/[^0-9]/g, "");

            var $jogosInputs = $(".jogos .jogo");
            $jogosInputs.each(function (i, e) {
                var ternoJogo = 0;
                var $inputP = $(this);
                var $inputs = $inputP.find(".box-jogo input");
                var $ternoJogo = $inputP.find(".qj");

                $inputs.each(function (i, e) {
                    var $inputC = $(this);
                    var inputVal = $inputC.val().replace(/[^0-9]/g, "");

                    switch (inputVal) {
                        case n1:
                        case n2:
                        case n3:
                        case n4:
                        case n5:
                            ternoJogo++;
                            break;
                    }
                });

                $ternoJogo.val(ternoJogo);
            });

            $t.updateTotalTernos();
        },
        updateTotalTernos: function () {
            var ternoTotal = 0;
            var $jogosTernos = $(".jogos .jogo .qj");
            $jogosTernos.each(function (i, e) {
                var valTerno = $(this).val().replace(/[^0-9]/g, "");
                var compareTerno = $("#qt").val().replace(/[^0-9]/g, "");

                if ((compareTerno !== "") && (valTerno === compareTerno)) {
                    ternoTotal++;
                }
            });
            $("#vt").val(ternoTotal);
        }
    };
    return $t;
}());